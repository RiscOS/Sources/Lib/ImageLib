# Copyright 1997 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for PNGLib
#
# ***********************************
# ***    C h a n g e   L i s t    ***
# ***********************************
# Date       Name         Description
# ----       ----         -----------
# 1997-06-25 BAL          Created.
# 2001-06-13 ADH          Wipes o and z directories as well as contents.
#

#
# Paths
#
EXP_HDR = <export$dir>
LIBDIR = <Lib$Dir>

#
# Generic options:
#
MKDIR   = do mkdir -p
AS      = objasm
CP      = copy
CC      = cc
CM      = cmhg
RM      = remove
LD      = link
LB      = libfile
WIPE    = x wipe

CCFLAGS = -c -depend !Depend -ffa -IC:zlib ${THROWBACK} -DPNG_ARM_NEON_IMPLEMENTATION=0 -DRISCOS
CPFLAGS = ~cfr~v
LBFLAGS = -c
WFLAGS  = ~CFR~V

#
# Rule patterns
#
.SUFFIXES: .o_ro .o_wo .o_roz .o_woz .oz .c

.c.o_ro:;  ${CC} $(CCFLAGS)     -o $@ $< -DPNG_NO_WRITE_SUPPORTED
.c.o_wo:;  ${CC} $(CCFLAGS)     -o $@ $< -DPNG_NO_READ_SUPPORTED
.c.o_roz:; ${CC} $(CCFLAGS) -zM -o $@ $< -DPNG_NO_WRITE_SUPPORTED
.c.o_woz:; ${CC} $(CCFLAGS) -zM -o $@ $< -DPNG_NO_READ_SUPPORTED
.c.oz:;    ${CC} $(CCFLAGS) -zM -o $@ $<
.c.o:;     ${CC} $(CCFLAGS)     -o $@ $<

#
# Program specific options:
#
COMPONENT = PNG

o.libpng-ro:   @.o_ro.png @.o_ro.pngerror @.o_ro.pngget @.o_ro.pngmem @.o_ro.pngpread \
        @.o_ro.pngread @.o_ro.pngrio @.o_ro.pngrtran @.o_ro.pngrutil @.o_ro.pngset @.o_ro.pngtrans local_dirs
        ${LB} $(LBFLAGS) -o $@ @.o_ro.png @.o_ro.pngerror @.o_ro.pngget @.o_ro.pngmem \
        @.o_ro.pngpread @.o_ro.pngread @.o_ro.pngrio @.o_ro.pngrtran @.o_ro.pngrutil @.o_ro.pngset @.o_ro.pngtrans

o.libpng-wo:   @.o_wo.pngwio @.o_wo.pngwrite @.o_wo.pngwtran @.o_wo.pngwutil @.o_wo.png @.o_wo.pngerror @.o_wo.pngget @.o_wo.pngmem \
        @.o_wo.pngset @.o_wo.pngtrans \
        @.o_wo.pngwio @.o_wo.pngwrite @.o_wo.pngwtran @.o_wo.pngwutil local_dirs
        ${LB} $(LBFLAGS) -o $@ @.o_wo.png @.o_wo.pngerror @.o_wo.pngget @.o_wo.pngmem \
        @.o_wo.pngset @.o_wo.pngtrans \
        @.o_wo.pngwio @.o_wo.pngwrite @.o_wo.pngwtran @.o_wo.pngwutil

o.libpng-roz:   @.o_roz.png @.o_roz.pngerror @.o_roz.pngget @.o_roz.pngmem @.o_roz.pngpread \
        @.o_roz.pngread @.o_roz.pngrio @.o_roz.pngrtran @.o_roz.pngrutil @.o_roz.pngset @.o_roz.pngtrans local_dirs
        ${LB} $(LBFLAGS) -o $@ @.o_roz.png @.o_roz.pngerror @.o_roz.pngget @.o_roz.pngmem \
        @.o_roz.pngpread @.o_roz.pngread @.o_roz.pngrio @.o_roz.pngrtran @.o_roz.pngrutil @.o_roz.pngset @.o_roz.pngtrans

o.libpng-woz:   @.o_woz.pngwio @.o_woz.pngwrite @.o_woz.pngwtran @.o_woz.pngwutil @.o_woz.png @.o_woz.pngerror @.o_woz.pngget @.o_woz.pngmem \
        @.o_woz.pngset @.o_woz.pngtrans \
        @.o_woz.pngwio @.o_woz.pngwrite @.o_woz.pngwtran @.o_woz.pngwutil local_dirs
        ${LB} $(LBFLAGS) -o $@ @.o_woz.png @.o_woz.pngerror @.o_woz.pngget @.o_woz.pngmem \
        @.o_woz.pngset @.o_woz.pngtrans \
        @.o_woz.pngwio @.o_woz.pngwrite @.o_woz.pngwtran @.o_woz.pngwutil

o.libpng-lib: @.o.pngwio @.o.pngwrite @.o.pngwtran @.o.pngwutil @.o.png @.o.pngerror @.o.pngget @.o.pngmem \
        @.o.pngset @.o.pngtrans @.o.pngpread \
        @.o.pngread @.o.pngrio @.o.pngrtran @.o.pngrutil \
        @.o.pngwio @.o.pngwrite @.o.pngwtran @.o.pngwutil local_dirs
        ${LB} $(LBFLAGS) -o $@ @.o.pngwio @.o.pngwrite @.o.pngwtran @.o.pngwutil @.o.png @.o.pngerror @.o.pngget @.o.pngmem \
        @.o.pngset @.o.pngtrans @.o.pngpread \
        @.o.pngread @.o.pngrio @.o.pngrtran @.o.pngrutil

o.libpng-lzm: @.oz.pngwio @.oz.pngwrite @.oz.pngwtran @.oz.pngwutil @.oz.png @.oz.pngerror @.oz.pngget @.oz.pngmem \
        @.oz.pngset @.oz.pngtrans @.oz.pngpread \
        @.oz.pngread @.oz.pngrio @.oz.pngrtran @.oz.pngrutil \
        @.oz.pngwio @.oz.pngwrite @.oz.pngwtran @.oz.pngwutil local_dirs
        ${LB} $(LBFLAGS) -o $@ @.oz.pngwio @.oz.pngwrite @.oz.pngwtran @.oz.pngwutil @.oz.png @.oz.pngerror @.oz.pngget @.oz.pngmem \
        @.oz.pngset @.oz.pngtrans @.oz.pngpread \
        @.oz.pngread @.oz.pngrio @.oz.pngrtran @.oz.pngrutil

#
# build the library:
#
all install export_libs: o.libpng-lzm o.libpng-lib o.libpng-ro o.libpng-wo o.libpng-roz o.libpng-woz
	@| Done

clean:
	${WIPE} o ${WFLAGS}
	${WIPE} oz ${WFLAGS}
	${WIPE} o_ro ${WFLAGS}
	${WIPE} o_wo ${WFLAGS}
	${WIPE} o_roz ${WFLAGS}
	${WIPE} o_woz ${WFLAGS}
	@echo ${COMPONENT}: cleaned

export: export_${PHASE}

export_hdrs:
	@| Nothing to do

local_dirs:
	${MKDIR} o
	${MKDIR} oz
	${MKDIR} o_ro
	${MKDIR} o_wo
	${MKDIR} o_roz
	${MKDIR} o_woz

# Dynamic dependencies:
